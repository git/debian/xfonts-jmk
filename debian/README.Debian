xfonts-jmk for Debian
---------------------

The most-used font in this package is Neep, which is an extremely legible
character cell font that's particularly useful for xterms, Emacs, and
similar programs.  It's the most readable character cell font that I've
found and is much clearer at many resolutions, at least in my opinion,
than the default misc-fixed fonts.

Thanks to the work of Derek Upham, the Neep font has Unicode (ISO-10646-1)
support in the 6x13, 8x15, and 10x20 sizes and can therefore be used in
UTF-8 locales.  The glyphs missing from the original upstream Neep font
are taken from the misc-fixed ISO-10646-1 font, so there is at times a
slight mismatch in style, but it's mostly not noticeable.

To display glyphs from Asian character sets, xterm automatically attempts
to find a double-wide font that corresponds to its normal font.  Since
Neep does not provide an Asian double-wide font, this will fail by default
in an xterm using Neep and all Asian characters will be displayed as solid
rectangles.  However, you can set the double-wide font separately and
using the misc-fixed double-wide font works fine.

For example, I use:

XTerm*VT100*font: *-jmk-neep-medium-r-normal-*-*-180-*-*-*-*-iso10646-1

in my ~/.Xresources file to use Neep for xterms.  So to configure a
corresponding Asian double-wide font, I added:

XTerm*VT100*wideFont: -misc-fixed-medium-r-normal-ja-18-*-*-*-*-*-iso10646-1

to ~/.Xresources.  The corresponding command-line option is -fw.

In order to test Unicode support, download:

    http://www.cl.cam.ac.uk/~mgk25/ucs/examples/UTF-8-demo.txt

and view it in an xterm.  You should be able to see all of the characters
in this file while using a Neep font, including the Japanese at the end of
the file if you have the wideFont configured correctly.

 -- Russ Allbery <rra@debian.org>, Wed, 25 Jul 2007 13:26:15 -0700
